import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import store from "@/store";
Vue.use(Router);

const router = new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
      props: true,
      meta: { requiresAuth: true }
    },
    {
      path: "/todo/:idList/:id",
      name: "Todo",
      meta: { requiresAuth: true },
      props: true,
      component: () =>
        import("./views/Todo"),
    },
    {
      path: "/todo/:idList",
      name: "TodoList",
      meta: { requiresAuth: true },
      props: true,
      component: () =>
        import( "./views/TodoList"),
    },
    {
      path: "/login",
      name: "login",
      component: () =>
        import(
        "./views/Login.vue")
    },
    {
      path: "/signup",
      name: "signup",
      component: () =>
        import(
        "./views/SignUp.vue")
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.user) {
      next({
        name: "login",
        query: { redirect: to.fullPath }
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
