Projet TodoLists :
API Flask + Vue JS 

Lancer le projet en mode dev : 

    1) a)
     Accèder à l'API Flask :
        cd Flask-API 
        . venv/bin/activate 
        python3 api.py
        
        Lancer votre navigateur sur cette route : localhost:5000/lists

    b) Toutes les routes sont visibles et utilisables dans la doc : localhost:5000/api/docs

    c) Les data par défaut se trouve dans Flask-API/app/services/todosService.py

    2) a) 
     Accèder au front Vue :
        cd vuejs-todo
        npm install
        npm run serve
        
        Lancer votre navigateur sur cette route : localhost:8080


Lancer le projet avec Docker compose :
    docker-compose up -d --build


Utilisation de l'application : 

En arrivant sur l'application localhost:8080, il faut se créer un compte puis se loger avec pour accèder au contenu des pages. 
Les user sont stockés mais pas la connexion. Lorsque la page est reload, vous devez vous reconnecter.  

Vous pouvez créer une todolist et y insérer un todo à faire. Vous pouvez modifier le todo en cliquant dessus. (en vrai j'arrivais à ouvrir un seul input à la fois lorsqu'il y avait plein de todo du coup j'ai contourné le problème).

Improve de mon projet : 
 - Une meilleure utilisation des stores : utiliser VueX pour passer des variables plus simplement
