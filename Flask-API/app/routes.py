from app import api

from app.resources.helloworld import HelloWorldResource, HelloWorldResourceNameToken, HelloWorldResourceNameURL, HelloWorldResourceNames
from app.resources.login import LoginResource,RegisterResource

from app.resources.todoslist import TodoListManagementResource, TodoListManagementResourceByID, TodoManagementResource

# Hello World
api.add_resource(HelloWorldResource, '/api/helloworld')
api.add_resource(HelloWorldResourceNameToken, '/api/hello')
api.add_resource(HelloWorldResourceNameURL, '/api/hello/<string:name>')
api.add_resource(HelloWorldResourceNames, '/api/hello/<int:count>')

# Login
api.add_resource(LoginResource, '/login')
api.add_resource(RegisterResource, '/account')

# Todos app

# Todo lists
api.add_resource(TodoListManagementResource, '/lists')
api.add_resource(TodoListManagementResourceByID, '/lists/<int:list_id>')

# Todo
api.add_resource(TodoManagementResource, '/lists/todos/<int:list_id>/<int:todo_id>')
