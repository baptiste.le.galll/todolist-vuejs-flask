from flask import request
from flask_restful import Resource, reqparse, abort

from typing import Dict, List, Any

from app.services.todosService import TODOS

class TodoListManagementResource(Resource):

    def get(self) -> Dict[str, Any]:
        """
        Return all the todolist
        ---
        tags:
            - Flask API
        responses:
            200:
                description: JSON representing all the elements of the todos service
        """
        return TODOS, 200

    def put(self) -> Dict[str, Any]:
        """

        Create the content of a todolist
        ---
        tags:
            - Flask API
        parameters:
            - in: body
              name: attributes
              description: The name and creation date of the task to create
              schema:
                type: object
                required:
                    - name
                    - created_on
                properties:
                  name:
                    type: string
                  created_on:
                    type: string
        responses:
            201:
                description: JSON representing created todo
            400:
                description: The parameters are missing or are not correct
        """
        body_parser = reqparse.RequestParser(bundle_errors=True) # Throw all the elements that has been filled uncorrectly
        body_parser.add_argument('name', type=str, required=True, help="Missing the name of the task")
        body_parser.add_argument('created_on', type=str, required=True, help="Missing the creation date of the task")
        args = body_parser.parse_args(strict=True) # Accepted only if these two parameters are strictly declared in body else raise exception
        try:
            id = getFirstMissingList()
            name = args['name']
            created_on = args['created_on']
            todo = {}
            todo['idList'] = id
            todo['name'] = name
            todo['created_on'] = created_on
            todo['tasks_list'] = []
            TODOS.insert(id, todo)
            return todo, 201
        except:
            abort(400)

class TodoListManagementResourceByID(Resource):

    def get(self, list_id: int) -> Dict[str, Any]:
        """
        Get the content of a todolist
        ---
        tags:
            - Flask API
        parameters:
            - in: path
              name: list_id
              description: The id of the todo to get
              required: true
              type: string
        responses:
            200:
                description: JSON representing the todo
            404:
                description: The todo does not exist
        """
        abort_if_todo_list_doesnt_exist(list_id)
        return TODOS[list_id], 200, {'content-type': 'application/json','Access-Control-Allow-Origin': '*'}

    def delete(self, list_id: int) -> Dict[str, Any]:
        """
        Delete the content of a todolist
        ---
        tags:
            - Flask API
        parameters:
            - in: path
              name: list_id
              description: The id of the todo to delete
              required: true
              type: string
        responses:
            200:
                description: JSON representing the todos
            404:
                description: The todo does not exist
        """
        for el in TODOS:
           if(el['idList'] == list_id):
                todo_list = el
        TODOS.remove(todo_list)

        return TODOS, 200,  {'content-type': 'application/json','Access-Control-Allow-Origin': '*'}

    def put(self, list_id: 0) -> Dict[str, Any]:
        """

        Create the content of a todo in list_id
        ---
        tags:
            - Flask API
        parameters:
            - in: path
              name: list_id
              description: The id of the todo list to insert
              required: true
              type: string
            - in: body
              name: attributes
              description: The name and creation date of the task to create
              schema:
                type: object
                required:
                    - name
                    - created_on
                properties:
                  name:
                    type: string
                  created_on:
                    type: string
        responses:
            201:
                description: JSON representing created todo
            400:
                description: The parameters are missing or are not correct
        """
        body_parser = reqparse.RequestParser(bundle_errors=True) # Throw all the elements that has been filled uncorrectly
        body_parser.add_argument('name', type=str, required=True, help="Missing the name of the task")
        body_parser.add_argument('created_on', type=str, required=True, help="Missing the creation date of the task")
        args = body_parser.parse_args(strict=True) # Accepted only if these two parameters are strictly declared in body else raise exception
        try:
            id = getFirstMissingID(list_id)
            name = args['name']
            created_on = args['created_on']
            todo = {}
            todo['id'] = id
            todo['name'] = name
            todo['created_on'] = created_on
            TODOS[list_id]["tasks_list"].insert(id, todo)
            return TODOS[list_id], 201, {'content-type': 'application/json','Access-Control-Allow-Origin': '*'}
        except:
            abort(400)

    def patch(self, list_id: int) -> Dict[str, Any]:
        """
        Update the name of list_id
        ---
        tags:
            - Flask API
        parameters:
            - in: path
              name: list_id
              description: The id of the todo to update
              required: true
              type: string
            - in: body
              name: attributes
              description: The updated name and/or the creation date of the task
              schema:
                type: object
                properties:
                  name:
                    type: string
                  created_on:
                    type: string
        responses:
            202:
                description: JSON representing updated todo if new data has been given by the body
            400:
                description: The parameters are missing or are not correct
            404:
                description: The todo does not exist
        """
        abort_if_todo_list_doesnt_exist(list_id)
        body_parser = reqparse.RequestParser()
        body_parser.add_argument('name', type=str, required=False, help="Missing the name of the task")
        body_parser.add_argument('created_on', type=str, required=False, help="Missing the creation date of the task")
        args = body_parser.parse_args(strict=False)
        try:
            task = TODOS[list_id]
            name = args['name']
            created_on = args['created_on']
            if name != None:
                task['name'] = name
            if created_on != None:
                task['created_on'] = created_on
            TODOS[list_id] = task
            return TODOS, 202 # Accepted, updated or not if putting the same data
        except:
            abort(400)


class TodoManagementResource(Resource):

    def get(self,list_id: int, todo_id: int) -> Dict[str, Any]:
        """
        Get the content of a todo in the todolist service
        ---
        tags:
            - Flask API
        parameters:
            - in: path
              name: list_id
              description: The id of the todolist to get
              required: true
              type: string
            - in: path
              name: todo_id
              description: The todo_id of the todolist to get
              required: true
              type: string

        responses:
            200:
                description: JSON representing the todo
            404:
                description: The todo does not exist
        """
        return TODOS[list_id]["tasks_list"][todo_id], 200

    def delete(self, list_id: int, todo_id: int) -> Dict[str, Any]:
        """
        Delete the content of a todo in the todolist service
        ---
        tags:
            - Flask API
        parameters:
            - in: path
              name: list_id
              description: The id of the todoList to delete
              required: true
              type: string
            - in: path
              name: todo_id
              description: The id of the todo to delete
              required: true
              type: string
        responses:
            200:
                description: JSON representing the todos
            404:
                description: The todo does not exist
        """

        for el in TODOS[list_id]['tasks_list']:
            if(el['id'] == todo_id):
                todo_list = el

        TODOS[list_id]["tasks_list"].remove(todo_list)
        return TODOS, 200, {'content-type': 'application/json','Access-Control-Allow-Origin': '*'}


    def patch(self, list_id: int, todo_id: int) -> Dict[str, Any]:
        """
        Update the content of a todo in the list_id
        ---
        tags:
            - Flask API
        parameters:
            - in: path
              name: list_id
              description: The id of the todolist to update
              required: true
              type: string
            - in: path
              name: todo_id
              description: The todo_id of the todolist to update
              required: true
              type: string
            - in: body
              name: attributes
              description: The updated name and/or the creation date of the task
              schema:
                type: object
                properties:
                  name:
                    type: string
                  created_on:
                    type: string
        responses:
            202:
                description: JSON representing updated todo if new data has been given by the body
            400:
                description: The parameters are missing or are not correct
            404:
                description: The todo does not exist
        """
        body_parser = reqparse.RequestParser()
        body_parser.add_argument('name', type=str, required=False, help="Missing the name of the task")
        body_parser.add_argument('created_on', type=str, required=False, help="Missing the creation date of the task")
        args = body_parser.parse_args(strict=False)
        try:
            task = TODOS[list_id]["tasks_list"][todo_id]
            name = args['name']
            created_on = args['created_on']
            if name != None:
                task['name'] = name
            if created_on != None:
                task['created_on'] = created_on
            TODOS[list_id]["tasks_list"][todo_id] = task
            return TODOS[list_id]["tasks_list"][todo_id], 202, {'content-type': 'application/json','Access-Control-Allow-Origin': '*'} # Accepted, updated or not if putting the same data
        except:
            abort(400)

def get_list_ids() -> List[int]:
    return list(map(lambda todo_list: todo_list['idList'], TODOS))

def abort_if_todo_list_doesnt_exist(list_id: int):
    list_ids = get_list_ids()
    if list_id not in list_ids:
        abort(404,  message="Cannot find the TODO with id {}".format(list_id))

def abort_if_todo_list_already_exist(list_id: int):
    list_ids = get_list_ids()
    if list_id in list_ids:
        abort(404,  message="TODO with id {} already exists".format(list_id))

def abort_if_todo_list_has_negative_id(list_id: int):
    if list_id < 0:
        abort(404,  message="TODO cannot have negative id value")

def getFirstMissingList() -> int:
    list_ids = get_list_ids()
    i = len(list_ids)
    if(i == 0 or i==1):
        return i
    else:
        i=1
    while (i < len(list_ids)):
        if (TODOS[i-1]['idList'] == TODOS[i]['idList'] - 1):
            i+=1
        else:
            break
    return i

def get_todo_ids(list_id: int) -> List[int]:
    return list(map(lambda todo: todo['id'], TODOS[list_id]["tasks_list"]))

def abort_if_todo_doesnt_exist(todo_id: int, list_id: int):
    todo_ids = get_todo_ids()
    if todo_id not in todo_ids:
        abort(404,  message="Cannot find the TODO with id {}".format(todo_id))

def abort_if_todo_already_exist(todo_id: int, list_id: int):
    todo_ids = get_todo_ids()
    if todo_id in todo_ids:
        abort(404,  message="TODO with id {} already exists".format(todo_id))

def getFirstMissingID(list_id) -> int:
    todo_ids = get_todo_ids(list_id)
    i = len(todo_ids)
    if(i == 0 or i == 1):
        return i
    else:
        i=1
        while (i < len(todo_ids)):
            if (TODOS[list_id]["tasks_list"][i-1]['id'] == TODOS[list_id]["tasks_list"][i]['id'] - 1):
                i+=1
            else:
                break
    return i
