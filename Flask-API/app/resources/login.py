from flask import request
from flask_restful import Resource, reqparse, abort
import jwt
from app.services.userService import USER

class LoginResource(Resource):

    def post(self):
        body_parser = reqparse.RequestParser()
        body_parser.add_argument('user', type=str, required=True, help="Missing the login of the user")
        body_parser.add_argument('pwd', type=str, required=True, help="Missing the password associated to the user login")
        args = body_parser.parse_args(strict=True) # Accepted only if these two parameters are strictly declared in body else raise exception
        try:
            user = args['user']
            password_hash = args['pwd']
            return login(user, password_hash)
        except:
            abort(400)

class RegisterResource(Resource):
    def post(self):
        body_parser = reqparse.RequestParser()
        body_parser.add_argument('user', type=str, required=True, help="Missing the login of the user")
        body_parser.add_argument('pwd', type=str, required=True, help="Missing the password associated to the user login")
        args = body_parser.parse_args(strict=True) # Accepted only if these two parameters are strictly declared in body else raise exception
        try:
            user = args['user']
            password_hash = args['pwd']
            users = {}
            users['user'] = user
            users['pwd'] = password_hash
            USER.insert(len(USER),users)
            return { "message" : "Logged in successfully", "status": 200}, 200
        except:
            abort(400)

# Hash for "password"
def login(login, hash):
    encode_jwt = jwt.encode({'login':login}, 'lemotdepassetropsecurise', algorithm='HS256')
    if any(user['user'] == login and user['pwd'] == hash for user in USER):
        return { "message" : "Logged in successfully", "token":encode_jwt, "data":login, "status": 200}, 200
    else:
        return { "message" : "Cannot log in, check your credentials and retry", "status":401}, 401
